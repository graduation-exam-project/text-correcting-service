import language_check

tool = language_check.LanguageTool('en-US')


class _DataParser:
    def __init__(self, err):
        self.position = [err.fromx, err.tox]
        self.message = err.msg
        self.correct = err.replacements

    def parse(self):
        return {
            "position": self.position,
            "message": self.message,
            "correct": self.correct
        }


class GrammarChecker:
    def __init__(self, text):
        self.text = text

    @staticmethod
    def __check(text):
        return tool.check(text)

    @staticmethod
    def __parse_checked_data(data):
        array = []
        for err in data:
            parser = _DataParser(err)
            array.append(parser.parse())
        return array

    def get_checked_data(self):
        check = self.__check(self.text)
        return self.__parse_checked_data(check)

